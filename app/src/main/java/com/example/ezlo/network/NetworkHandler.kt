package com.example.ezlo.network

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import androidx.annotation.RequiresApi
import com.example.ezlo.utils.EzloApplication

class NetworkHandler {

    companion object {

        @RequiresApi(Build.VERSION_CODES.M)
        fun isInternetAvailable(): Boolean {
            (EzloApplication.getCtx().getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager).run {
                return this.getNetworkCapabilities(this.activeNetwork)?.hasCapability(
                    NetworkCapabilities.NET_CAPABILITY_INTERNET
                ) ?: false
            }
        }
    }
}