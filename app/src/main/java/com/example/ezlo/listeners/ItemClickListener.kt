package com.example.ezlo.listeners

interface ItemClickListener {

    fun onDeviceClicked(position: Int)

    fun onLongClicked(position: Int)

    fun onEditClicked(position: Int)

}