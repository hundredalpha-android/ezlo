package com.example.ezlo.view_model

import android.app.ProgressDialog
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.ezlo.model.DeviceData
import com.example.ezlo.repository.DeviceRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class DeviceViewModel(private val repository: DeviceRepository) : ViewModel() {

    init {
        viewModelScope.launch(Dispatchers.IO) {
            repository.getDevices(null)
        }
    }

    fun getDevicesFromLocalDB() {
        repository.getDevicesFromLocalDB()
    }

    val devices: LiveData<MutableList<DeviceData>>
        get() = repository.deviceList
}