package com.example.ezlo.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.ezlo.model.DeviceData

@Database(
    entities = [DeviceData::class],
    version = 1
)

abstract class DatabaseSetup : RoomDatabase() {

    abstract fun deviceDao(): DeviceDao

    companion object {
        @Volatile
        private var INSTANCE: DatabaseSetup? = null

        fun getDatabase(context: Context): DatabaseSetup {
            if (INSTANCE == null) {
                synchronized(this) {
                    INSTANCE = Room.databaseBuilder(
                        context,
                        DatabaseSetup::class.java,
                        "ezlo"
                    )
                        .allowMainThreadQueries()
                        .build()
                }
            }
            return INSTANCE!!
        }
    }

}