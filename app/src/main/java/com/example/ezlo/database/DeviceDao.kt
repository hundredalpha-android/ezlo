package com.example.ezlo.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.ezlo.model.DeviceData

@Dao
interface DeviceDao {

    @Insert
    fun insertDevices(deviceData: DeviceData)

    @Query("Update device set home = :home where PK_Device = :pk_device")
    fun updateDevice(home: String, pk_device: Int)

    @Query("SELECT * FROM device")
    fun getDevices(): MutableList<DeviceData>

    @Query("delete from device where PK_Device = :pk_device")
    fun deleteDevice(pk_device: Int)

    @Query("delete from device")
    fun deleteAllDevices()

}