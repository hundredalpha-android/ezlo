package com.example.ezlo.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import org.jetbrains.annotations.NotNull
import java.io.Serializable

@Entity(tableName = "device")
data class DeviceData(
    @NotNull
    val home: String?,
    @NotNull
    val Firmware: String?,
    @NotNull
    val InternalIP: String?,
    @NotNull
    val LastAliveReported: String?,
    @NotNull
    val MacAddress: String?,
    @NotNull
    val PK_Account: Int?,
    @NotNull
    val PK_Device: Int?,
    @NotNull
    val PK_DeviceSubType: Int?,
    @NotNull
    val PK_DeviceType: Int?,
    @NotNull
    val Platform: String?,
    @NotNull
    val Server_Account: String?,
    @NotNull
    val Server_Device: String?,
    @NotNull
    val Server_Event: String?

) : Serializable {
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0
}