package com.example.ezlo.repository

import android.app.ProgressDialog
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.ezlo.config.API
import com.example.ezlo.database.DatabaseSetup
import com.example.ezlo.model.DeviceData
import com.example.ezlo.network.NetworkHandler
import com.example.ezlo.utils.EzloApplication

class DeviceRepository(private val api: API, private val roomDatabase: DatabaseSetup) {

    private val deviceLiveData = MutableLiveData<MutableList<DeviceData>>()
    val deviceList: LiveData<MutableList<DeviceData>>
        get() = deviceLiveData

    suspend fun getDevices(progressDialog: ProgressDialog?) {
        if (NetworkHandler.isInternetAvailable()) {
            val result = api.getDevices()
            if (result.body() != null) {
                roomDatabase.deviceDao().deleteAllDevices()
                for (i in 0 until result.body()?.Devices?.size!!) {
                    val value = i + 1
                    val home = "Home Number $value"
                    roomDatabase.deviceDao().insertDevices(
                        DeviceData(
                            home,
                            result.body()?.Devices!![i].Firmware,
                            result.body()?.Devices!![i].InternalIP,
                            result.body()?.Devices!![i].LastAliveReported,
                            result.body()?.Devices!![i].MacAddress,
                            result.body()?.Devices!![i].PK_Account,
                            result.body()?.Devices!![i].PK_Device,
                            result.body()?.Devices!![i].PK_DeviceSubType,
                            result.body()?.Devices!![i].PK_DeviceType,
                            result.body()?.Devices!![i].Platform,
                            result.body()?.Devices!![i].Server_Account,
                            result.body()?.Devices!![i].Server_Device,
                            result.body()?.Devices!![i].Server_Event
                        )
                    )
                }

                getDevicesFromLocalDB()
                progressDialog?.dismiss()
            }
        } else {
            progressDialog?.dismiss()
            getDevicesFromLocalDB()
            Toast.makeText(EzloApplication.getCtx(), "No Internet Connectivity", Toast.LENGTH_SHORT)
                .show()
        }
    }

    fun getDevicesFromLocalDB() {
        val deviceList = roomDatabase.deviceDao().getDevices()
        deviceLiveData.postValue(deviceList)
    }

}
