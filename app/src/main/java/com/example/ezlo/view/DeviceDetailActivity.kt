package com.example.ezlo.view

import android.os.Bundle
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.example.ezlo.R
import com.example.ezlo.config.setImages
import com.example.ezlo.database.DatabaseSetup
import com.example.ezlo.databinding.ActivityDetailBinding
import com.example.ezlo.model.DeviceData
import com.example.ezlo.utils.SharedPreferenceHandler

class DeviceDetailActivity : AppCompatActivity() {

    private lateinit var binding: ActivityDetailBinding
    private val database = DatabaseSetup.getDatabase(this)
    private var pkDevice = 0;
    var prefs: SharedPreferenceHandler? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_detail)
        initialize()

        binding.update.setOnClickListener {
            database.deviceDao().updateDevice(binding.etHomeNo.text.toString(), pkDevice)
            prefs?.setFromDetail(true)
            finish()
        }
    }

    private fun initialize() {
        prefs = SharedPreferenceHandler(applicationContext)
        val device = intent.getSerializableExtra("device") as? DeviceData
        val from = intent.getStringExtra("from")
        pkDevice = device?.PK_Device!!;

        if (from.equals("edit")) {
            binding.homeNo.visibility = GONE
            binding.etHomeNo.visibility = VISIBLE
            binding.update.visibility = VISIBLE
            binding.etHomeNo.setText(device.home)
        }

        binding.homeNo.text = device.home
        binding.sNo.text = "SN:" + device.PK_Device.toString()
        binding.macAdd.text = "MAC Address: " + device.MacAddress
        binding.firmware.text = "Firmware: " + device.Firmware
        binding.model.text = "Model: " + device.Platform
        setImages(device, binding.img)
    }
}

