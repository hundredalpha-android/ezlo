package com.example.ezlo.view

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.ezlo.R
import com.example.ezlo.listeners.ItemClickListener
import com.example.ezlo.config.setImages
import com.example.ezlo.model.DeviceData
import de.hdodenhof.circleimageview.CircleImageView

class DeviceAdapter(
    private val context: Context?,
    private val list: List<DeviceData>,
    private val itemClickListener: ItemClickListener
) :
    RecyclerView.Adapter<DeviceAdapter.ViewHolder>() {

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v =
            LayoutInflater.from(context).inflate(R.layout.device_list_item, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    @SuppressLint("NotifyDataSetChanged")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val results = list[position]
        holder.house_no.text = results.home
        holder.s_no.text = results.PK_Device.toString()
        setImages(results, holder.img)

        holder.itemView.setOnClickListener {
            notifyDataSetChanged()
            itemClickListener.onDeviceClicked(position)
        }

        holder.edit.setOnClickListener {
            notifyDataSetChanged()
            itemClickListener.onEditClicked(position)
        }

        holder.itemView.setOnLongClickListener {
            notifyDataSetChanged()
            itemClickListener.onLongClicked(position)
            return@setOnLongClickListener true
        }
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return list.size
    }

    //the class is holding the list view
    class ViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {

        var img = itemView.findViewById<CircleImageView>(R.id.img)
        var house_no = itemView.findViewById<TextView>(R.id.home_no)
        var s_no = itemView.findViewById<TextView>(R.id.s_no)
        var edit = itemView.findViewById<ImageView>(R.id.edit)
    }

}
