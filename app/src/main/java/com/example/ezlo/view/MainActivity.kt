package com.example.ezlo.view

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.ezlo.R
import com.example.ezlo.listeners.ItemClickListener
import com.example.ezlo.config.initializeDialog
import com.example.ezlo.database.DatabaseSetup
import com.example.ezlo.databinding.ActivityMainBinding
import com.example.ezlo.model.DeviceData
import com.example.ezlo.repository.DeviceViewModelFactory
import com.example.ezlo.utils.EzloApplication
import com.example.ezlo.utils.SharedPreferenceHandler
import com.example.ezlo.view_model.DeviceViewModel

class MainActivity : AppCompatActivity(), ItemClickListener {

    private lateinit var binding: ActivityMainBinding
    private lateinit var deviceViewModel: DeviceViewModel
    private lateinit var progressDialog: ProgressDialog
    private lateinit var deviceAdapter: DeviceAdapter
    private var deviceDataList: MutableList<DeviceData> = ArrayList()
    var prefs: SharedPreferenceHandler? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        val repository = (application as EzloApplication).deviceRepository
        deviceViewModel =
            ViewModelProvider(
                this,
                DeviceViewModelFactory(repository)
            )[DeviceViewModel::class.java]

        initialize()
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onResume() {
        super.onResume()
        if (prefs?.getFromDetail() == true) {
            prefs?.setFromDetail(false)
            deviceViewModel.getDevicesFromLocalDB()
            deviceAdapter.notifyDataSetChanged()
        }
    }

    private fun initialize() {
        prefs = SharedPreferenceHandler(applicationContext)
        progressDialog = initializeDialog(this)
        progressDialog.show()
        viewItems()
    }

    private fun viewItems() {
        deviceViewModel.devices.observe(this, {
            deviceDataList = it
            deviceAdapter = DeviceAdapter(this, deviceDataList, this)
            binding.deviceView.adapter = deviceAdapter
            binding.deviceView.layoutManager = LinearLayoutManager(this)
            binding.deviceView.addItemDecoration(
                DividerItemDecoration(
                    this,
                    LinearLayoutManager.VERTICAL
                )
            )

            progressDialog.dismiss()
        })
    }

    override fun onLongClicked(position: Int) {
        val database = DatabaseSetup.getDatabase(this)
        showAlertDialog(database, position)
    }

    override fun onDeviceClicked(position: Int) {
        switchToDetail(position, "view");
    }

    override fun onEditClicked(position: Int) {
        switchToDetail(position, "edit");
    }

    private fun switchToDetail(pos: Int, from: String) {
        val device: DeviceData = deviceDataList[pos]
        val intent = Intent(this, DeviceDetailActivity::class.java)
        intent.putExtra("device", device)
        intent.putExtra("from", from)
        startActivity(intent)
    }

    @SuppressLint("NotifyDataSetChanged")
    fun showAlertDialog(
        databaseSetup: DatabaseSetup,
        position: Int
    ) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Alert")
        builder.setMessage("Do you want to delete this device?")

        builder.setPositiveButton("Yes") { dialogInterface, which ->
            val device: DeviceData = deviceDataList[position]
            databaseSetup.deviceDao().deleteDevice(device.PK_Device!!)
            deviceViewModel.getDevicesFromLocalDB()
            deviceAdapter.notifyDataSetChanged()
            dialogInterface.dismiss()
        }

        builder.setNegativeButton("Cancel") { dialogInterface, which ->
            dialogInterface.dismiss()
        }
        val alertDialog: AlertDialog = builder.create()
        alertDialog.setCancelable(false)
        alertDialog.show()
    }
}