package com.example.ezlo.utils

import android.content.Context
import android.content.SharedPreferences

class SharedPreferenceHandler(context: Context) {

    private val sharedPref: SharedPreferences =
        context.getSharedPreferences("ezlo", Context.MODE_PRIVATE)
    private val editor: SharedPreferences.Editor = sharedPref.edit()

    fun setFromDetail(value: Boolean) {
        editor.putBoolean("fromDetail", value)
        editor.commit()
    }

    fun getFromDetail(): Boolean {
        return sharedPref.getBoolean("fromDetail", false)
    }

}