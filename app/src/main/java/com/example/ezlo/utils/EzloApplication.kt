package com.example.ezlo.utils

import android.app.Application
import android.content.Context
import com.example.ezlo.config.API
import com.example.ezlo.config.RetrofitHelper
import com.example.ezlo.database.DatabaseSetup
import com.example.ezlo.repository.DeviceRepository

class EzloApplication : Application() {

    lateinit var deviceRepository: DeviceRepository

    companion object {
        private lateinit var appContext: Context
        fun getCtx(): Context {
            return appContext
        }
    }

    override fun onCreate() {
        super.onCreate()
        appContext = applicationContext;
        init()
    }

    private fun init(){
        val apiService = RetrofitHelper.getInstance().create(API::class.java)
        val database = DatabaseSetup.getDatabase(appContext)
        deviceRepository = DeviceRepository(apiService, database)
    }
}