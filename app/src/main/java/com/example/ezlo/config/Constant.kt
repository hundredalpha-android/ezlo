package com.example.ezlo.config

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.Context
import android.util.Log
import android.view.View
import android.widget.ImageView
import androidx.appcompat.app.AlertDialog
import com.example.ezlo.R
import com.example.ezlo.database.DatabaseSetup
import com.example.ezlo.model.DeviceData
import com.example.ezlo.view.DeviceAdapter
import com.google.android.material.snackbar.Snackbar

const val BASE_URL: String = "https://veramobile.mios.com/test_android/"
const val DEVICES: String = "items.test"

fun showSnackBar(view: View, message: String) {
    Snackbar.make(view, message, Snackbar.LENGTH_LONG).show()
}

fun initializeDialog(con: Context): ProgressDialog {
    val progressDialog = ProgressDialog(con)
    progressDialog.setMessage("Please Wait...")
    progressDialog.setCancelable(false)
    return progressDialog
}

fun setImages(deviceData: DeviceData, imageView: ImageView){
    when {
        deviceData.Platform.equals("Sercomm G450") ->
            imageView.setImageResource(R.drawable.vera_plus_big)

        deviceData.Platform.equals("Sercomm G550") ->
            imageView.setImageResource(R.drawable.vera_secure_big)

        else ->
            imageView.setImageResource(R.drawable.vera_edge_big)
    }
}



