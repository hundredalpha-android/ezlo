package com.example.ezlo.config

import com.example.ezlo.model.Device
import retrofit2.Response
import retrofit2.http.*

interface API {

    @GET(DEVICES)
    suspend fun getDevices(): Response<Device>

}